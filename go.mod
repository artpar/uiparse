module github.com/artpar/ui-parser

go 1.13

require (
	github.com/artpar/douceur v0.2.4
	github.com/artpar/go.uuid v1.2.0
	github.com/blevesearch/bleve/v2 v2.0.2
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.5.0
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/jinzhu/copier v0.2.5
	github.com/json-iterator/go v1.1.9 // indirect
	github.com/karrick/godirwalk v1.15.3
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/s8sg/goflow v0.1.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/vmarkovtsev/go-lcss v0.0.0-20181022055826-27bee2e750a5
	github.com/yosssi/gohtml v0.0.0-20200519115854-476f5b4b8047
	golang.org/x/net v0.0.0-20200822124328-c89045814202
	gopkg.in/go-playground/validator.v9 v9.31.0 // indirect
)
