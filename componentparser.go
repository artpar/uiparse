package main

import (
	"bufio"
	"bytes"
	"context"
	"crypto/md5"
	"errors"
	"fmt"
	"log"
	"os"
	"regexp"
	"sync"
	"time"
)

type ComponentParser struct {
	incomingChannel chan ParseComponentRequest
	outgoingChannel chan HtmlComponent
	wg              sync.WaitGroup
	depth           int
}

func NewComponentParser(workerCount int, incomingChannel chan ParseComponentRequest) *ComponentParser {
	inl := &ComponentParser{
		incomingChannel: incomingChannel,
		outgoingChannel: make(chan HtmlComponent, 4),
		wg:              sync.WaitGroup{},
	}
	for i := 0; i < workerCount; i++ {
		inl.wg.Add(1)
		go inl.ParseComponent()
	}
	return inl
}

func (i *ComponentParser) GetResultChannel() chan HtmlComponent {
	return i.outgoingChannel
}
func (i *ComponentParser) ParseComponent() {

	for parseRequest := range i.incomingChannel {
		log.Printf("New parse component request: [%s]", parseRequest.Path)
		start := time.Now()
		ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelFunc()
		components, e := i.ParseComponentWithContext(parseRequest, ctx)
		log.Printf("Parse component took [%d seconds]", time.Now().Unix()-start.Unix())
		CheckErr(e, "Failed to parse componens")

		for _, component := range components {
			i.outgoingChannel <- component
		}
	}
	i.wg.Done()
}

func (i *ComponentParser) ParseComponentWithContext(parseComponentRequest ParseComponentRequest, ctx context.Context) ([]HtmlComponent, error) {

	for {
		select {
		case <-ctx.Done():
			fmt.Println("Timed out")
			return []HtmlComponent{}, errors.New("timeout")
		default:
			components := parseComponent(parseComponentRequest, i.depth)
			return components, nil
		}

	}

}

func parseComponent(inlinedHtml ParseComponentRequest, depth int) []HtmlComponent {

	//log.Printf("Received inlined content %d bytes", len(inlinedHtml))
	var re = regexp.MustCompile(`<style[^>]*>([^<]+)</style>`)
	inlinedHtml.Html = re.ReplaceAllString(inlinedHtml.Html, ``)

	md5Hash := fmt.Sprintf("%x", md5.Sum([]byte(inlinedHtml.Html)))

	inlineFileWriter, err := os.OpenFile("out"+string(os.PathSeparator)+md5Hash+".html", os.O_CREATE, 0777)
	if err != nil {
		panic(err)
	}

	_, _ = inlineFileWriter.Write([]byte(inlinedHtml.Html))
	_ = inlineFileWriter.Sync()

	start := time.Now()

	htmlTree, err := BuildHtmlTreeStrings(bufio.NewReader(bytes.NewReader([]byte(inlinedHtml.Html))), depth, inlinedHtml.Path)

	log.Printf("Node tree generation took %d seconds", time.Now().Unix()-start.Unix())
	CheckErr(err, "Failed to generate html tree strings")
	//cssHMap = htmlTree.CssHashMap
	CheckErr(err, "3")

	return htmlTree.Components
}

func (i *ComponentParser) Done() {
	close(i.incomingChannel)
	i.wg.Wait()
	close(i.outgoingChannel)
}
