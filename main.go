package main

import (
	"bufio"
	"bytes"
	"container/heap"
	"crypto/md5"
	"crypto/sha512"
	"fmt"
	uuid "github.com/artpar/go.uuid"
	"github.com/yosssi/gohtml"

	"github.com/blevesearch/bleve/v2"
	//"github.com/derekparker/trie"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/copier"
	"github.com/karrick/godirwalk"
	log "github.com/sirupsen/logrus"
	"github.com/vmarkovtsev/go-lcss"
	"golang.org/x/net/html"
	"io"
	"io/ioutil"
	_ "net/http/pprof"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

type ParseComponentRequest struct {
	Html string
	Path string
}

type ThemeParse struct {
	// path to theme html/css files on dist
	themePath string

	// the max depth for html tree with unique structure to count as a component
	depth int

	// list of all files in the theme directory, populated after parse call
	fileMapByType map[string][]string
	// file content hash => file path map
	fileMapByHash map[string]string

	// css inliner used by the parser
	inliner *Inliner

	// links to all 3rd party css files included in the project
	allImportedCss []string

	// all identified unique component html
	components         []HtmlComponent
	index              bleve.Index
	componentMapById   map[string]HtmlComponent
	componentMapByHash map[string][]HtmlComponent
}

type HtmlIndexComponent struct {
	Keywords []string
	FileName string
}

type ComponentType string

const (
	ContainerComponent     ComponentType = "ContainerComponent"
	ContainerItemComponent               = "ContainerItemComponent"
	LeafComponent                        = "LeafComponent"
)

// One html component
type HtmlComponent struct {
	// hash of the inner html
	ContentHash string
	// random uuid4 assigned to every node at runtime
	Id string
	// path to the file on disk
	ParentFilePath string

	// component type is either:
	// Container (which can contain list of same leaf components)
	// ContainerItemComponent (Leafs for a container)
	// Leaf (don't need a container, usually not placed consecutively)
	ComponentType ComponentType

	// actual html code
	HtmlExample string

	// populate with relevant keywords for search
	Keywords []string

	ParentId        string
	ChildComponents []HtmlComponent
}

type InlineRequest struct {
	Html []byte
	Path string
}

func (p *ThemeParse) Parse() {
	log.Infof("Read theme [%v]", p.themePath)

	err := godirwalk.Walk(p.themePath, &godirwalk.Options{
		Callback: func(osPathname string, de *godirwalk.Dirent) error {
			//fmt.Printf("%s %s\n", de.ModeType(), osPathname)

			switch filepath.Ext(osPathname) {
			case ".html":
				p.fileMapByType["html"] = append(p.fileMapByType["html"], osPathname)
			case ".js":
				p.fileMapByType["js"] = append(p.fileMapByType["js"], osPathname)
			case ".css":
				p.fileMapByType["css"] = append(p.fileMapByType["css"], osPathname)
			case ".scss":
				p.fileMapByType["scss"] = append(p.fileMapByType["scss"], osPathname)
			default:
				//log.Errorf("Skipping file: %v", osPathname)
			}

			return nil
		},
		Unsorted: true, // (optional) set true for faster yet non-deterministic enumeration (see godoc)
	})
	CheckErr(err, "Failed to walk dir [%v]", p.themePath)

	themePathAbs, _ := filepath.Abs(p.themePath)
	cssMap := make(map[string]string)
	for _, file := range p.fileMapByType["css"] {
		file = strings.ReplaceAll(file, "/", string(os.PathSeparator))
		fileContents, err := ioutil.ReadFile(file)
		if err != nil {
			log.Printf("Failed to load css from [%v]: %v", file, err)
			continue
		}
		cssContents := string(fileContents)

		var re = regexp.MustCompile(`("[^"\\]*(?:\\.[^"\\]*)*")|/\*[^*]*\*+(?:[^/*][^*]*\*+)*/`)
		cssContents = re.ReplaceAllString(cssContents, `$1`)

		cssMap[file] = cssContents
		relativePath := strings.Split(file, themePathAbs)[1][1:]
		cssMap[relativePath] = cssContents
		log.Printf("Read css [%v]", relativePath)
	}

	//var cssHMap map[string]string
	htmlFileCount := len(p.fileMapByType["html"])
	log.Printf("HTML files: %d", htmlFileCount)
	allImportedCss := make([]string, 0)
	p.allImportedCss = allImportedCss

	go func() {
		for i, filePath := range p.fileMapByType["html"] {
			if strings.Index(filePath, "node_modules/") > -1 {
				continue
			}
			log.Printf("[ %d / %d ] Process file %v", i, htmlFileCount, filePath[len(themePathAbs):])
			file, err := os.OpenFile(filePath, os.O_RDONLY, 0600)
			CheckErr(err, "Failed to open file [%v]", filePath)

			//start := time.Now()
			htmlToInline, importedCss, err := InlineCssContent(file, cssMap)
			//log.Printf("inlinecsscontent took %d seconds", time.Now().Unix()-start.Unix())

			for _, css := range importedCss {
				if !Contains(p.allImportedCss, css) {
					p.allImportedCss = append(p.allImportedCss, css)
				}
			}

			htmlBytes, err := ioutil.ReadAll(htmlToInline)
			htmlBytesHash := fmt.Sprintf("%x", sha512.Sum512(htmlBytes))
			p.fileMapByHash[htmlBytesHash] = filePath

			//err = ioutil.WriteFile("temp.html", htmlBytes, 0777)
			//CheckErr(err, "1 [%v]", file)

			p.inliner.incomingChannel <- InlineRequest{
				Html: htmlBytes,
				Path: filePath,
			}

		}
		p.inliner.Done()
	}()

	p.components = make([]HtmlComponent, 0)
	//classChildrenCountMap := make(map[string]int)
	//childCountMap := make(map[string]int)
	_, err = os.Stat("out")
	if err != nil {
		err = os.Mkdir("out", 0666)
		CheckErr(err, "Failed to create cache directory: ./out")
	}

	componentParser := NewComponentParser(8, p.inliner.outgoingChannel)

	for comp := range componentParser.GetResultChannel() {
		//log.Printf("New html component: %v -> %v", comp.ContentHash, comp.Keywords)
		if !HtmlComponentContains(p.components, comp) {

			p.components = append(p.components, comp)

			if comp.ComponentType != LeafComponent {
				err = p.index.Index(comp.Id, HtmlIndexComponent{
					Keywords: comp.Keywords,
					FileName: filepath.Base(comp.ParentFilePath),
				})
				CheckErr(err, "Failed to index component")

			}

			p.componentMapById[comp.Id] = comp
			_, ok := p.componentMapByHash[comp.ContentHash]
			if !ok {
				p.componentMapByHash[comp.ContentHash] = make([]HtmlComponent, 0)
			}
			p.componentMapByHash[comp.ContentHash] = append(p.componentMapByHash[comp.ContentHash], comp)
		}
	}

	log.Printf("Finished reading all html files ")

}

func (p *ThemeParse) QueryComponents(searchRequest bleve.SearchRequest) (*bleve.SearchResult, error) {

	//matchQuery := bleve.NewS(searchRequest)

	//search := bleve.NewSearchRequest(searchRequest)
	results, err := p.index.Search(&searchRequest)

	return results, err
}

func (p *ThemeParse) GetComponentById(id string) HtmlComponent {
	return p.componentMapById[id]
}

func (p *ThemeParse) GetComponentsByHash(query string) ([]HtmlComponent, bool) {
	components, ok := p.componentMapByHash[query]
	return components, ok
}

func NewThemeParser(themePath string, depth int) *ThemeParse {

	fileMapByType := make(map[string][]string)
	fileMapByHash := make(map[string]string)
	fileMapByType["css"] = make([]string, 0)
	fileMapByType["js"] = make([]string, 0)
	fileMapByType["html"] = make([]string, 0)
	fileMapByType["scss"] = make([]string, 0)

	mapping := bleve.NewIndexMapping()
	_ = os.RemoveAll("html-components.bleve")
	index, err := bleve.New("html-components.bleve", mapping)
	if err != nil {
		//fmt.Errorf(err)
		panic(err)
	}

	return &ThemeParse{
		themePath:          themePath,
		depth:              depth,
		inliner:            NewInliner(8),
		fileMapByType:      fileMapByType,
		componentMapById:   make(map[string]HtmlComponent),
		componentMapByHash: make(map[string][]HtmlComponent),
		fileMapByHash:      fileMapByHash,
		index:              index,
	}
}

func main() {

	themePath := os.Args[1]
	i, err := strconv.ParseInt(os.Args[2], 10, 32)
	if err != nil {
		i = 10
	}
	depth := int(i)
	//mostCommonSubStrings := make(map[string]bool)
	//htmlTagTrie := trie.New()
	//cssTagTrie := trie.New()

	themeParser := NewThemeParser(themePath, depth)
	go themeParser.Parse()

	//hashToHtml := make(map[string][]string)
	//classCountMap := make(map[string]int)

	router := gin.Default()

	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"GET", "POST"},
		AllowHeaders:     []string{"Origin"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	router.Static("/site", "site")

	router.GET("/api/components", func(c *gin.Context) {
		//c.JSON(200, components)

		c.Writer.WriteString("<!DOCTYPE html>\n")
		c.Writer.WriteString("<html>\n")
		c.Writer.WriteString("<head>\n")

		for _, importedCss := range themeParser.allImportedCss {
			c.Writer.WriteString(fmt.Sprintf("<link href=\"%s\" rel='stylesheet' type='text/css' >\n", importedCss))
		}
		c.Writer.WriteString("</head>\n")
		c.Writer.WriteString("<body>\n")

		c.Writer.WriteString("<table>")
		c.Writer.WriteString("<thead>")
		c.Writer.WriteString("<tr>")
		c.Writer.WriteString("<th style='width: 50vw' >Preview</th>")
		c.Writer.WriteString("<th style='width: 50vw' >Code</th>")
		c.Writer.WriteString("</tr>")
		c.Writer.WriteString("</thead>")
		c.Writer.WriteString("<tbody>")
		for _, comp := range themeParser.components {

			c.Writer.WriteString("<tr>")
			c.Writer.WriteString("<td>")

			c.Writer.WriteString(comp.HtmlExample)
			c.Writer.WriteString("</td>")
			c.Writer.WriteString("<td>")
			c.Writer.WriteString("<textarea  style='width: 50vw; min-height: 300px;' >")
			c.Writer.WriteString(strings.ReplaceAll(strings.ReplaceAll(gohtml.Format(comp.HtmlExample), "<", "&lt;"), ">", "&gt;"))
			c.Writer.WriteString("</textarea>")

			c.Writer.WriteString("</td>")
			c.Writer.WriteString("\n")
			c.Writer.WriteString("<hr> <hr> \n")
		}
		c.Writer.WriteString("</table>")

		c.Writer.WriteString("</body>\n")
		c.Writer.WriteString("</html>\n")
	})

	router.GET("/api/source/:id", func(c *gin.Context) {
		//path := c.Query("path")
		index := c.Param("id")
		//if index == "" {
		//	index = "0"
		//}
		//i, _ := strconv.ParseInt(index, 10, 32)
		//idLessPath := RemoveIdsFromDomPath(path)

		component := themeParser.GetComponentById(index)

		c.Writer.WriteString("<!DOCTYPE html>\n")
		c.Writer.WriteString("<html>\n")
		c.Writer.WriteString("<head>\n")

		for _, importedCss := range themeParser.allImportedCss {
			c.Writer.WriteString(fmt.Sprintf("<link href=\"%s\" rel='stylesheet' type='text/css' >\n", importedCss))
		}
		c.Writer.WriteString("</head>\n")
		c.Writer.WriteString("<body>\n")

		c.Writer.WriteString(component.HtmlExample)

		c.Writer.WriteString("</body>\n")
		c.Writer.WriteString("</html>\n")

	})

	router.GET("/api/find", func(c *gin.Context) {

		query := c.Query("query")

		size := 10
		fromIndexVal := c.Query("from")
		fromIndex := int64(0)
		if fromIndexVal != "" {
			fromIndex, _ = strconv.ParseInt(fromIndexVal, 10, 64)
		}

		searchQuery := bleve.SearchRequest{
			Query: bleve.NewQueryStringQuery(query),
			Size:  size,
			From:  int(fromIndex),
		}

		results, err := themeParser.QueryComponents(searchQuery)

		components := make([]HtmlComponent, 0)
		for _, res := range results.Hits {
			comp := themeParser.GetComponentById(res.ID)
			components = append(components, comp)
		}

		if err != nil {
			c.Error(err)
		} else {
			c.JSON(200, gin.H{
				"Result":     results,
				"Components": components,
			})
		}

	})

	router.GET("/api/componentsbyhash", func(c *gin.Context) {

		query := c.Query("hash")
		results, _ := themeParser.GetComponentsByHash(query)

		c.JSON(200, gin.H{
			"Components": results,
		})

	})

	router.GET("/api/componentbyid", func(c *gin.Context) {

		query := c.Query("id")
		results := themeParser.GetComponentById(query)

		c.JSON(200, gin.H{
			"Component": results,
		})

	})

	//router.GET("/", func(c *gin.Context) {
	//
	//	fuzzy := htmlTagTrie.Root().Val()
	//	c.JSON(200, gin.H{
	//		"root": fuzzy,
	//	})
	//
	//})

	log.Printf("Starting server at 8089")
	log.Error(router.Run(":8089"))

}

var idRegex = regexp.MustCompile("(\\.[0-9a-z]+)")

func RemoveIdsFromDomPath(path string) string {
	return idRegex.ReplaceAllString(path, "")
}

func EndsWithAny(query string, subStrings []string) bool {
	for _, s := range subStrings {
		if query[len(query)-len(s):] == s {
			return true
		}
	}

	return false
}

func MatchesWithAny(query string, subStrings []string) bool {
	for _, s := range subStrings {
		if query == s {
			return true
		}
	}

	return false
}

type ClassStructureInfo struct {
	classCountMap map[string]int
}

func ProcessClassCount(classCountMap map[string]int) *ClassStructureInfo {

	classHeap := getHeap(classCountMap)

	sort.Stable(classHeap)
	for {
		l := classHeap.Len()
		if l < 1 {
			break
		}
		item := classHeap.Pop()
		val := item.(kv)
		log.Printf("Class [%v] => %d", val.Key, val.Value)
	}
	return &ClassStructureInfo{
		classCountMap: classCountMap,
	}
}

type kv struct {
	Key   string
	Value int
}

func getHeap(m map[string]int) *KVHeap {
	h := &KVHeap{}
	heap.Init(h)
	for k, v := range m {
		heap.Push(h, kv{k, v})
	}
	return h
}

type KVHeap []kv

func (h KVHeap) Len() int           { return len(h) }
func (h KVHeap) Less(i, j int) bool { return h[i].Value < h[j].Value }
func (h KVHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *KVHeap) Push(x interface{}) {
	*h = append(*h, x.(kv))
}

func (h *KVHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

// Contains tells whether a contains x.
func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// Contains tells whether a contains x.
func HtmlComponentContains(a []HtmlComponent, x HtmlComponent) bool {
	for _, n := range a {
		if x.ContentHash == n.ContentHash {
			return true
		}
	}
	return false
}

func NewId() string {
	id, _ := uuid.NewV4()
	return id.String()
}

func InlineCssContent(r io.Reader, cssMap map[string]string) (io.Reader, []string, error) {
	z := html.NewTokenizer(r)
	var err error
	var outString []byte
	srcPathFailed := make(map[string]bool)
	buff := bytes.NewBuffer(outString)
	importedCss := make([]string, 0)
	writer := bufio.NewWriter(buff)
	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			// log.Errorf("Error token [%v]", tt)
			break
		}

		token := z.Token()
		if token.Data == "link" {

			//foundReplace := false
			for _, attr := range token.Attr {
				if attr.Key == "href" {
					srcPath := attr.Val
					if srcPath[0:4] == "http" || srcPath[0:2] == "//" {
						importedCss = append(importedCss, srcPath)
						break
					}

					srcPath = strings.ReplaceAll(srcPath, "/", string(os.PathSeparator))

					cssContent, ok := cssMap[srcPath]
					if !EndsWithAny(srcPath, []string{".css"}) {
						continue
					}
					if !ok {
						if !srcPathFailed[srcPath] {
							log.Errorf("Failed to load css [%v]", srcPath)
							srcPathFailed[srcPath] = true
						}
						break
					}
					_, err = writer.WriteString(fmt.Sprintf("<style>\n%s</style>", cssContent))
					break
				}
			}

		} else if token.Data == "a" {
			newAttrs := make([]html.Attribute, 0)
			for _, attr := range token.Attr {
				if attr.Key != "class" {
					attr.Val = "#" + attr.Key
				}
				newAttrs = append(newAttrs, attr)

			}
			token.Attr = newAttrs
			_, err = writer.WriteString(token.String())

		} else if token.Type == html.TextToken {
			_, err = writer.WriteString(strings.TrimSpace(token.String()))

		} else {
			_, err = writer.WriteString(token.String())

		}

		if err != nil {
			log.Errorf("Failed to write [%v]: %v", token.String(), err)
			return bytes.NewReader(outString), importedCss, err
		}

	}
	err = writer.Flush()
	return bytes.NewReader(buff.Bytes()), importedCss, err
}

type HtmlTreeString struct {
	CssHashMap            map[string]string
	TagPathToHtmlMap      map[string][]string
	IdToHashMap           map[string]string
	AllStrings            []string
	ClassCountMap         map[string]int
	ChildrenCountMap      map[string]int
	ClassStringTree       map[string]int
	CssPathToHtmlMap      map[string][]string
	ClassChildrenCountMap map[string]int
	Components            []HtmlComponent
}

func NewHtmlTreeString() *HtmlTreeString {
	return &HtmlTreeString{
		AllStrings:       make([]string, 0),
		CssHashMap:       make(map[string]string),
		TagPathToHtmlMap: make(map[string][]string),
		ClassCountMap:    make(map[string]int),
		IdToHashMap:      make(map[string]string),
	}
}

func NewHtmlTreeStringWithParams(allStrings []string, cssHashMap map[string]string,
	tagPathToHtmlMap map[string][]string, cssPathToHtmlMap map[string][]string, idToHashMap map[string]string,
	classCountMap map[string]int, countMap map[string]int, classStringTree map[string]int,
	classChildrenCountMap map[string]int, components []HtmlComponent) *HtmlTreeString {
	return &HtmlTreeString{
		AllStrings:            allStrings,
		CssHashMap:            cssHashMap,
		TagPathToHtmlMap:      tagPathToHtmlMap,
		Components:            components,
		CssPathToHtmlMap:      cssPathToHtmlMap,
		IdToHashMap:           idToHashMap,
		ClassCountMap:         classCountMap,
		ClassChildrenCountMap: classChildrenCountMap,
		ChildrenCountMap:      countMap,
		ClassStringTree:       classStringTree,
	}
}

type Component struct {
	html string
}

func BuildHtmlTreeStrings(r io.Reader, maxDepth int, sourceFilePath string) (*HtmlTreeString, error) {

	treeStringsMap := make(map[string]bool)
	cssHashMap := make(map[string]string)
	tagPathToHtmlMap := make(map[string][]string)
	cssPathToHtmlMap := make(map[string][]string)

	originalHtmlWriterStack := make([]io.Writer, 0)
	normalizedHtmlWriterStack := make([]io.Writer, 0)

	idToHashMap := make(map[string]string)
	classCountMap := make(map[string]int)
	classChildrenCountMap := make(map[string]int)
	classStack := make([][]string, 0)
	classStringTree := make(map[string]int)
	//components := make([]Component, 0)
	currentComponentHeight := 0
	//initialCloseStackCountHit := 1
	//componentHeightThresholdAdder := 0
	//nextComponentMinimumHeight := initialCloseStackCountHit
	components := make([]HtmlComponent, 0)
	htmlHashToIdMap := make(map[string][]string)

	var currentOriginalHtmlWriter io.Writer
	var currentNormalizedHtmlWriter io.Writer

	originalBufferMap := make(map[string]*bytes.Buffer)
	normalizedBufferMap := make(map[string]*bytes.Buffer)

	currentOriginalHtmlWriter = &bytes.Buffer{}
	currentNormalizedHtmlWriter = &bytes.Buffer{}

	z := html.NewTokenizer(r)
	stringStack := make([]string, 0)
	idStack := make([]string, 0)
	childrenCountMap := make(map[string]int)
	childrenCountStack := make([]int, 0)
	childrenComponentStack := make([][]HtmlComponent, 0)

	startTokenStack := make([]NormalizedHtmlToken, 0)

	var err error
	for {
		tt := z.Next()
		if tt == html.ErrorToken {
			break
		}

		htmlToken := z.Token()
		normalizedToken := NormalizeHtmlToken(htmlToken)

		if tt == html.StartTagToken {
			currentComponentHeight = 0
			//nextComponentMinimumHeight = initialCloseStackCountHit

			startTokenStack = append(startTokenStack, normalizedToken)

			if len(childrenCountStack) > 0 {
				childrenCountStack[len(childrenCountStack)-1] = childrenCountStack[len(childrenCountStack)-1] + 1
			}
			childrenCountStack = append(childrenCountStack, 0)
			childrenComponentStack = append(childrenComponentStack, make([]HtmlComponent, 0))

			var normalizedChildWriter io.Writer
			normalizedChildWriter = &bytes.Buffer{}

			var originalChildWriter io.Writer
			originalChildWriter = &bytes.Buffer{}

			originalHtmlWriterStack = append(originalHtmlWriterStack, currentOriginalHtmlWriter)
			normalizedHtmlWriterStack = append(normalizedHtmlWriterStack, currentNormalizedHtmlWriter)

			currentOriginalHtmlWriter = originalChildWriter
			currentNormalizedHtmlWriter = normalizedChildWriter

			_, err = currentOriginalHtmlWriter.Write([]byte(normalizedToken.UnNormalizedToken.String()))
			CheckErr(err, "Failed to write to normalized html buffer")
			_, err = currentNormalizedHtmlWriter.Write([]byte(normalizedToken.Token.String()))
			CheckErr(err, "Failed to write to original html buffer")

			styleAttribute := GetAttribute(normalizedToken.Token.Attr, "style")
			styleHash := ""
			if styleAttribute.Val != "" {
				styleHash = fmt.Sprintf("%x", md5.Sum([]byte(styleAttribute.Val)))
				cssHashMap[styleHash] = styleAttribute.Val
			}

			classAttribute := GetAttribute(normalizedToken.Token.Attr, "class")

			if classAttribute.Val != "" {
				classNames := strings.Split(classAttribute.Val, " ")

				classes := make([]string, 0)
				for _, className := range classNames {
					trimmedClassName := strings.TrimSpace(className)
					if trimmedClassName == "" {
						continue
					}
					classes = append(classes, className)
				}
				sort.Strings(classes)
				classCountMap[strings.Join(classes, ",")] += 1
				classStack = append(classStack, classes)

			} else {
				classStack = append(classStack, []string{})
			}

			newId := NewId()
			idToHashMap[newId] = "unassigned"
			tag := normalizedToken.Token.Data
			tag = tag + "." + newId
			idStack = append(idStack, newId)
			stringStack = append(stringStack, tag)
			originalBufferMap["<"+strings.Join(stringStack, "<")] = originalChildWriter.(*bytes.Buffer)
			normalizedBufferMap["<"+strings.Join(stringStack, "<")] = normalizedChildWriter.(*bytes.Buffer)

		} else if tt == html.EndTagToken {
			currentComponentHeight += 1
			if len(stringStack) < 1 {
				log.Errorf("No element to pop")
				continue
			}

			startToken := startTokenStack[len(startTokenStack)-1]
			startTokenStack = startTokenStack[:len(startTokenStack)-1]

			childrenCount := childrenCountStack[len(childrenCountStack)-1]

			_, err = currentOriginalHtmlWriter.Write([]byte(normalizedToken.UnNormalizedToken.String()))
			CheckErr(err, "Failed to write end tag")

			_, err = currentNormalizedHtmlWriter.Write([]byte(normalizedToken.Token.String()))
			CheckErr(err, "Failed to write end tag")

			tagIdAttribute := GetAttribute(startToken.UnNormalizedToken.Attr, "id")

			id := idStack[len(idStack)-1]
			parentId := "html"
			if len(idStack) > 1 {
				parentId = idStack[len(idStack)-2]
			}

			domPath := "<" + strings.Join(stringStack, "<")

			normalizedOuterHTML := normalizedBufferMap[domPath].String()
			originalOuterHTML := originalBufferMap[domPath].String()

			normalizedHtmlHash := Sha521Hash(normalizedOuterHTML)

			idToHashMap[id] = normalizedHtmlHash

			originalBufferMap[domPath] = nil
			normalizedBufferMap[domPath] = nil

			delete(originalBufferMap, domPath)
			delete(normalizedBufferMap, domPath)

			childrenCountMap[domPath] = childrenCount

			currentNormalizedHtmlWriter = normalizedHtmlWriterStack[len(normalizedHtmlWriterStack)-1]
			currentOriginalHtmlWriter = originalHtmlWriterStack[len(originalHtmlWriterStack)-1]

			normalizedHtmlWriterStack = normalizedHtmlWriterStack[:len(normalizedHtmlWriterStack)-1]
			originalHtmlWriterStack = originalHtmlWriterStack[:len(originalHtmlWriterStack)-1]

			classStackTop := classStack[len(classStack)-1]

			newStrings := generateStrings(stringStack, maxDepth)
			for str := range newStrings {
				treeStringsMap[str] = true
				if nil == tagPathToHtmlMap[str] {
					tagPathToHtmlMap[str] = make([]string, 0)
				}
				//tagPathToHtmlMap[str] = append(tagPathToHtmlMap[str], outerHTML)
			}

			_, ok := htmlHashToIdMap[normalizedHtmlHash]
			if !ok {
				htmlHashToIdMap[normalizedHtmlHash] = []string{id}
			} else {
				htmlHashToIdMap[normalizedHtmlHash] = append(htmlHashToIdMap[normalizedHtmlHash], id)

			}

			var siblingComponents []HtmlComponent
			if len(childrenComponentStack) > 1 {
				siblingComponents = childrenComponentStack[len(childrenComponentStack)-2]
			}

			newUniqueChild := true
			for _, child := range siblingComponents {
				if child.ContentHash == normalizedHtmlHash {
					newUniqueChild = false
					break
				}
			}

			if newUniqueChild {
				_, err = currentOriginalHtmlWriter.Write([]byte(originalOuterHTML))
				_, err = currentNormalizedHtmlWriter.Write([]byte(normalizedOuterHTML))
				CheckErr(err, "Failed to write to parent html buffer")

			}

			childrenComponents := childrenComponentStack[len(childrenComponentStack)-1]
			componentType := IdentifyComponentType(childrenComponents, siblingComponents)

			if componentType == ContainerComponent {
				for _, child := range childrenComponents {
					child.ComponentType = ContainerItemComponent
				}
			}

			component := HtmlComponent{
				Id:              id,
				ParentId:        parentId,
				ContentHash:     normalizedHtmlHash,
				ParentFilePath:  sourceFilePath,
				ComponentType:   componentType,
				HtmlExample:     originalOuterHTML,
				Keywords:        classStackTop,
				ChildComponents: childrenComponents,
			}

			if tagIdAttribute.Val != "" {
				component.Keywords = append(component.Keywords, tagIdAttribute.Val)
			}

			components = append(components, component)
			if len(childrenComponentStack) > 1 {
				childrenComponentStack[len(childrenComponentStack)-2] = append(childrenComponentStack[len(childrenComponentStack)-2], component)
				childrenComponentStack = childrenComponentStack[:len(childrenComponentStack)-1]
			}

			stringStackTop := stringStack[len(stringStack)-1]
			topId := idStack[len(idStack)-1]
			if !BeginsWith(stringStackTop, normalizedToken.Token.Data) {
				log.Errorf("Stack top mismatch [%v] [%v] [%v]", stringStackTop, normalizedToken.Token.Data)
			}
			for len(stringStack) > 1 && normalizedToken.Token.Data+"."+topId != stringStackTop {
				stringStack = stringStack[:len(stringStack)-1]
				stringStackTop = stringStack[len(stringStack)-1]
			}

			newClassStrings := make([]string, 0)
			if len(classStackTop) > 0 {
				classChildrenCountMap[strings.Join(classStackTop, ",")] = childrenCount
				newClassStrings = GenerateStringsDoubleStack(classStack, 7)
			}

			for _, cstr := range newClassStrings {
				classStringTree[cstr] += 1
			}

			//for _, newClassString := range newClassStrings {
			//	if !Contains(cssPathToHtmlMap[newClassString], htmlHash) {
			//cssPathToHtmlMap[newClassString] = append(cssPathToHtmlMap[newClassString], outerHTML)
			//}
			//}

			classStack = classStack[:len(classStack)-1]
			childrenCountStack = childrenCountStack[:len(childrenCountStack)-1]
			stringStack = stringStack[:len(stringStack)-1]
			idStack = idStack[:len(idStack)-1]
		} else if tt == html.SelfClosingTagToken {

			normalizedChildWriter := &bytes.Buffer{}
			originalChildWriter := &bytes.Buffer{}

			normalizedHtmlWriterStack = append(normalizedHtmlWriterStack, currentNormalizedHtmlWriter)
			currentNormalizedHtmlWriter = normalizedChildWriter
			_, err := currentNormalizedHtmlWriter.Write([]byte(normalizedToken.Token.String()))

			originalHtmlWriterStack = append(originalHtmlWriterStack, currentOriginalHtmlWriter)
			currentOriginalHtmlWriter = originalChildWriter

			_, err = normalizedChildWriter.Write([]byte(normalizedToken.Token.String()))
			_, err = originalChildWriter.Write([]byte(normalizedToken.UnNormalizedToken.String()))

			CheckErr(err, "Failed to write to html buffer")

			//normalizedOuterHtml := normalizedChildWriter.String()
			originalOuterHtml := originalChildWriter.String()
			// htmlHash := Sha521Hash(outerHTML)

			stringStack = append(stringStack, normalizedToken.Token.Data)

			newStrings := generateStrings(stringStack, maxDepth)

			currentOriginalHtmlWriter = originalHtmlWriterStack[len(originalHtmlWriterStack)-1]
			originalHtmlWriterStack = originalHtmlWriterStack[:len(originalHtmlWriterStack)-1]

			currentNormalizedHtmlWriter = normalizedHtmlWriterStack[len(normalizedHtmlWriterStack)-1]
			normalizedHtmlWriterStack = normalizedHtmlWriterStack[:len(normalizedHtmlWriterStack)-1]

			for str := range newStrings {
				treeStringsMap[str] = true

				if nil == tagPathToHtmlMap[str] {
					tagPathToHtmlMap[str] = make([]string, 0)
				}
				tagPathToHtmlMap[str] = append(tagPathToHtmlMap[str], originalOuterHtml)

			}

			stringStack = stringStack[:len(stringStack)-1]

		} else if tt == html.CommentToken {
			//log.Infof("Ignoring comment token")
		} else if tt == html.DoctypeToken {
			//log.Info("Ignoring doc type token")
		} else if tt == html.TextToken {

			originalChildWriter := &bytes.Buffer{}
			normalizedChildWriter := &bytes.Buffer{}

			originalHtmlWriterStack = append(originalHtmlWriterStack, currentOriginalHtmlWriter)
			currentOriginalHtmlWriter = originalChildWriter

			normalizedHtmlWriterStack = append(normalizedHtmlWriterStack, currentNormalizedHtmlWriter)
			currentNormalizedHtmlWriter = normalizedChildWriter

			innerText := normalizedToken.Token.String()
			if len(strings.Trim(innerText, " \t\n")) > 0 {

				_, err = currentNormalizedHtmlWriter.Write([]byte(innerText))
				_, err = currentOriginalHtmlWriter.Write([]byte(normalizedToken.UnNormalizedToken.String()))

				CheckErr(err, "Failed to write to html buffer")

				normalizedOuterHtml := normalizedChildWriter.String()
				originalOuterHtml := originalChildWriter.String()

				normalizedHtmlHash := Sha521Hash(normalizedOuterHtml)

				stringStack = append(stringStack, ":text."+normalizedHtmlHash)
				newStrings := generateStrings(stringStack, maxDepth)

				currentNormalizedHtmlWriter = normalizedHtmlWriterStack[len(normalizedHtmlWriterStack)-1]
				normalizedHtmlWriterStack = normalizedHtmlWriterStack[:len(normalizedHtmlWriterStack)-1]

				currentOriginalHtmlWriter = originalHtmlWriterStack[len(originalHtmlWriterStack)-1]
				originalHtmlWriterStack = originalHtmlWriterStack[:len(originalHtmlWriterStack)-1]

				_, err = currentNormalizedHtmlWriter.Write([]byte(innerText))
				_, err = currentOriginalHtmlWriter.Write([]byte(normalizedToken.UnNormalizedToken.String()))
				CheckErr(err, "Failed to write to parent html buffer")

				for str := range newStrings {
					treeStringsMap[str] = true

					if nil == tagPathToHtmlMap[str] {
						tagPathToHtmlMap[str] = make([]string, 0)
					}
					tagPathToHtmlMap[str] = append(tagPathToHtmlMap[str], originalOuterHtml)

				}
				stringStack = stringStack[:len(stringStack)-1]
			}

		}

	}

	allStrings := make([]string, 0)
	for k := range treeStringsMap {
		allStrings = append(allStrings, k)
	}

	return NewHtmlTreeStringWithParams(allStrings, cssHashMap, tagPathToHtmlMap,
		cssPathToHtmlMap, idToHashMap, classCountMap,
		childrenCountMap, classStringTree, classChildrenCountMap,
		components), nil

}

func IdentifyComponentType(childComponents []HtmlComponent, parentComponentChildren []HtmlComponent) ComponentType {
	//hasRepeatedChild := false

	childComponentHashMap := make(map[string]bool)
	for _, child := range childComponents {
		if _, ok := childComponentHashMap[child.ContentHash]; ok {
			//hasRepeatedChild = true
			return ContainerComponent
		}
		childComponentHashMap[child.ContentHash] = true
	}
	return LeafComponent

}

type NormalizedHtmlToken struct {
	Token             html.Token
	UnNormalizedToken html.Token
}

func NormalizeHtmlToken(token html.Token) NormalizedHtmlToken {

	tokenCopy := html.Token{}
	err := copier.Copy(&tokenCopy, &token)
	if err != nil {
		panic(err)
	}

	switch token.Type {
	case html.ErrorToken:
	case html.EndTagToken:
	case html.CommentToken:
		token.Data = ""
	case html.DoctypeToken:
	case html.TextToken:
		if len(strings.TrimSpace(token.Data)) > 0 {
			token.Data = "#Text"
		}
	case html.SelfClosingTagToken:
		fallthrough
	case html.StartTagToken:
		for _, attr := range token.Attr {
			switch attr.Key {
			case "id":
				fallthrough
			case "src":
				fallthrough
			case "alt":
				fallthrough
			case "href":
				attr.Val = "#" + attr.Key
			}
		}
	}
	return NormalizedHtmlToken{
		Token:             token,
		UnNormalizedToken: tokenCopy,
	}
}

func Sha521Hash(data string) string {
	is := md5.New()
	is.Write([]byte(data))
	return fmt.Sprintf("%x", is.Sum(nil))
}

func BeginsWith(bigger string, starter string) bool {
	return len(bigger) > len(starter) && bigger[0:len(starter)] == starter
}

func GetAttribute(attributes []html.Attribute, s string) html.Attribute {
	for _, a := range attributes {
		if a.Key == s {
			return a
		}
	}
	return html.Attribute{
		Namespace: "",
		Key:       s,
		Val:       "",
	}
}

type AllString struct {
	allStrings    []string
	allStringsMap map[string]bool
}

func MostCommonSubstrings(allStrings AllString, newStrings []string) []string {
	mostCommonSubstrings := make([]string, 0)

	for _, allString := range allStrings.allStrings {
		for _, newStrin := range newStrings {
			lcs := lcss.LongestCommonSubstring([]byte(allString), []byte(newStrin))
			lcsStr := string(lcs)
			if len(strings.Split(lcsStr, "<")) < 3 {
				continue
			}
			mostCommonSubstrings = append(mostCommonSubstrings, "<"+strings.Trim(lcsStr, "<"))
		}
	}
	return mostCommonSubstrings
}

func StringArrayDiff(allStrings AllString, htmlStrings []string) []string {
	var diff []string

	// Loop two times, first to find slice1 strings not in slice2,
	// second loop to find slice2 strings not in slice1
	for _, s1 := range htmlStrings {
		found := false
		if allStrings.allStringsMap[s1] {
			found = true
		}
		// String not found. We add it to return slice
		if !found {
			diff = append(diff, s1)
		}
	}

	return diff
}

func StringArrayDiffFromMap(allStrings AllString, htmlStrings map[string]int) []string {
	var diff []string

	// Loop two times, first to find slice1 strings not in slice2,
	// second loop to find slice2 strings not in slice1
	for s1 := range htmlStrings {
		found := false
		if allStrings.allStringsMap[s1] {
			found = true
		}
		// String not found. We add it to return slice
		if !found {
			diff = append(diff, s1)
		}
	}

	return diff
}

func CheckErr(err error, args ...interface{}) {
	if err != nil {
		if len(args) > 0 {
			format, ok := args[0].(string)
			if !ok {
				log.Error(err)
				log.Errorf("%v", args...)
			} else {
				args[0] = err
				log.Errorf(format+" [%v]: "+": %v", args...)
			}
		}
	}
}

func generateStrings(stringStack []string, depth int) map[string]bool {
	newStrings := make(map[string]bool)
	stackStr := "<" + strings.Join(stringStack, "<")
	newStrings[stackStr] = true
	return newStrings
}

func GenerateStringsDoubleStack(stringStack [][]string, depth int) []string {

	if depth == 0 {
		return []string{}
	}
	if len(stringStack) == 0 {
		return []string{}
	}

	remainingStack := stringStack[:len(stringStack)-1]
	parentStrings := GenerateStringsDoubleStack(remainingStack, depth-1)

	top := stringStack[len(stringStack)-1]
	if len(top) == 0 {
		return parentStrings
	}

	if len(parentStrings) == 0 {
		return []string{"<" + strings.Join(top, ",")}
	}

	for _, pStr := range parentStrings {
		parentStrings = append(parentStrings, pStr+"<"+strings.Join(top, ","))
	}

	return parentStrings
}
