package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/artpar/douceur/inliner"
	"log"
	"sync"
	"time"
)

type Inliner struct {
	incomingChannel chan InlineRequest
	outgoingChannel chan ParseComponentRequest
	wg              sync.WaitGroup
}

func NewInliner(workerCount int) *Inliner {
	inl := &Inliner{
		incomingChannel: make(chan InlineRequest, 4),
		outgoingChannel: make(chan ParseComponentRequest, 4),
		wg:              sync.WaitGroup{},
	}
	for i := 0; i < workerCount; i++ {
		inl.wg.Add(1)
		go inl.Inline()
	}
	return inl
}

func (i *Inliner) Inline() {

	for htmlContent := range i.incomingChannel {
		log.Printf("New inline request %d bytes [%v]", len(htmlContent.Html), htmlContent.Path)
		start := time.Now()
		ctx, cancelFunc := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancelFunc()
		s, e := i.InlineWithContext(string(htmlContent.Html), ctx)
		log.Printf("Inline took [%d seconds]", time.Now().Unix()-start.Unix())
		CheckErr(e, "Failed to inline: %v", htmlContent.Path)
		if e != nil {
			log.Printf("Waiting for next inline request")
			continue
		}

		log.Printf("New result from inliner")
		i.outgoingChannel <- ParseComponentRequest{
			Html: s,
			Path: htmlContent.Path,
		}
		log.Printf("Waiting for next inline request")

	}
	i.wg.Done()
}
func (i *Inliner) InlineWithContext(htmlContent string, ctx context.Context) (string, error) {

	for {
		select {
		case <-ctx.Done():
			fmt.Println("Timed out")
			return "", errors.New("timeout")
		default:
			s, e := inliner.Inline(htmlContent)
			return s, e
		}

	}

}

func (i *Inliner) Done() {
	close(i.incomingChannel)
	i.wg.Wait()
	close(i.outgoingChannel)
}
